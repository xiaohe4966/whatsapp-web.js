const WebSocket = require('ws');

// 创建 WebSocket 连接
// const ws = new WebSocket('ws://chat3.he4966.cn:1819/xiaohe');
// 获取命令行参数中的 WebSocket 地址
const wsUrl = process.argv[2];

if (!wsUrl) {
    console.error('请提供 WebSocket 地址作为参数。');
    process.exit(1);
}


// 创建 WebSocket 连接
const ws = new WebSocket(wsUrl);

// 连接成功后的回调
ws.on('open', function open() {
    console.log(`连接成功：${wsUrl}`);

    // 构建要发送的消息对象
    const message = {
        msg_id: 212,
        acc_id: 122,
        msg: '测试消息',
        from_id: 1,
        from_name: '小何',
        cy_id: 2399,
        acc_type: 'wechat',
        msg_type: 0,
        cy_name: 9
    };

    // 发送消息
    ws.send(JSON.stringify(message));
});

// 接收服务器消息的回调
ws.on('message', function incoming(data) {
    console.log('收到消息：', data.toString());
});

// 连接错误的回调
ws.on('error', function error(err) {
    console.error('连接错误：', err);
});

// 连接关闭的回调
ws.on('close', function close() {
    console.log('连接关闭');
});